import router from '../../router'

export default {
  namespaced: true,
  state: () => ({
    auth: false,
    baseUrl: 'http://dev-api.rnsb.ru/api',
    headers: { "Content-Type": "application/json" },
    authData: null,
    userData: null,
    identityData: null,
    companyData: null,
    userImages: null,
    message: null
  }),
  getters: {},
  mutations: {
    setHeaders (state, payload) {
      Object.assign(state.headers, payload);
    },
    setAuth (state, payload) {
      state.auth = payload
    },
    setAuthData (state, payload) {
      state.authData = payload
    },
    setUserData (state, payload) {
      state.userData = payload
    },
    setIdentityData (state, payload) {
      state.identityData = payload
    },
    setCompanyData (state, payload) {
      state.companyData = payload
    },
    setUserRoles (state, payload) {
      state.userRoles = payload
    },
    setMessage (state, payload) {
      state.message = payload?.detail ? payload.detail : null
    },
    setUserImages (state, payload) {
      state.userImages = payload
    },
  },
  actions: {
    async login ({ state, commit, dispatch }, payload) {
      try {
        const response = await fetch(`${state.baseUrl}/auth/personal/`, {
          method: 'POST',
          headers: state.headers,
          body: JSON.stringify(payload)
        })
        const data = await response.json()
        if (data.success === 'true') {
          if (data.data.key) {
            commit('setHeaders', { Authorization: data.data.key })
            commit('setAuthData', data.data)
            localStorage.setItem('authData', JSON.stringify(data.data))
            dispatch('getIdentity')
            commit('setAuth', true)
          }
        } else {
          commit('setMessage', data)
        }
      } catch (error) {
        console.log(error)
      }
    },
    async refresh ({ state, commit, dispatch }, payload) {
      try {
        const response = await fetch(`${state.baseUrl}/auth/refresh/`, {
          method: 'POST',
          headers: state.headers,
          body: JSON.stringify(payload)
        })
        const data = await response.json()
        if (data.success === 'true') {
          if (data.data.key) {
            commit('setHeaders', { Authorization: data.data.key })
            commit('setAuthData', data.data)
            localStorage.setItem('authData', JSON.stringify(data.data))
            dispatch('getIdentity')
            commit('setAuth', true)
          }
        } else {
          dispatch('logout')
          router.push('/')
        }
      } catch (error) {
        console.log(error)
      }
    },
    loguot ({ commit }) {
      commit('setAuth', false)
      commit('setAuthData', null)
      localStorage.removeItem('authData')
    },
    async getIdentity ({ state, commit }) {
      const identityId = state.authData.identity
      try {
        const response = await fetch(`${state.baseUrl}/account/identity/${identityId}`, {
          method: 'GET',
          headers: state.headers,
        })
        const data = await response.json()
        if (data.success === 'true') {
          commit('setAuth', true)
          commit('setIdentityData', data.data)
        } else {
          commit('setAuth', false)
          if (state.auchData) {
            dispatch('refresh', state.auchData.refresh_key)
          }
        }
      } catch (error) {
        console.log(error)
      }
    },
    async getUser ({ state, commit }) {
      const userId = state.identityData.user
      try {
        const response = await fetch(`${state.baseUrl}/account/user/${userId}`, {
          method: 'GET',
          headers: state.headers,
        })
        const data = await response.json()
        if (data.success === 'true') {
          commit('setUserData', data.data)
        }
      } catch (error) {
        console.log(error)
      }
    },

    async getCompany ({ state, commit }) {
      const companyId = state.identityData.company
      try {
        const response = await fetch(`${state.baseUrl}/account/company/${companyId}`, {
          method: 'GET',
          headers: state.headers,
        })
        const data = await response.json()
        if (data.success === 'true') {
          commit('setCompanyData', data.data)
        }
      } catch (error) {
        console.log(error)
      }
    },
    async getUserRoles ({ state, commit }) {
      try {
        const response = await fetch(`${state.baseUrl}/account/myroles/`, {
          method: 'GET',
          headers: state.headers,
        })
        const data = await response.json()
        if (data.success === 'true') {
          commit('setUserRoles', data.data.results)
        }
      } catch (error) {
        console.log(error)
      }
    },
    async imageUpload ({ state, commit, dispatch }, payload) {
      commit('setHeaders', { "Content-Type": "multipart/form-data" })
      try {
        const response = await fetch(`${state.baseUrl}/media/image/upload/`, {
          method: 'POST',
          headers: state.headers,
          body: payload
        })
        const data = await response.json()
        if (data.success === 'true') {
          dispatch('getUserImage')
        }
      } catch (error) {
        console.log(error)
      }
    },
    async getUserImage ({ state, commit }) {
      try {
        const response = await fetch(`${state.baseUrl}/media/image/account__user/${state.userData.id}/`, {
          method: 'GET',
          headers: state.headers,
        })
        const data = await response.json()
        console.log('🚀 ~ getUserImage ~ data', data)
        if (data.success === 'true') {
          commit('setUserImages', data.data.results)
        }
      } catch (error) {
        console.log(error)
      }
    },
  }
}