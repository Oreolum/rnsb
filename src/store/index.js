import { createStore } from 'vuex'
import main from './modules/main'

const store = createStore()

store.registerModule('main', main)

export default store
