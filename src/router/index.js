import { createRouter, createWebHistory } from 'vue-router'

const r = [
  { path: '/', view: 'Home', name: 'Главная' },
  { path: '/profile', view: 'Profile', name: 'Профиль' },
]

const routes = r.map((item) => {
  return {
    path: item.path,
    name: item.name,
    component: () => import(`../views/${item.view}.vue`)
  }
})

const router = createRouter({
  history: createWebHistory(),
  routes,
})

export default router
